
#!/bin/bash
#The code requires ttnctl to be installed and signed in
#For more details visit https://www.thethingsnetwork.org/docs/network/cli/quick-start.html
while :
do
   old_time=$(ttnctl gateways status gatewayid --router-id ttn-router-eu | awk  '/Last/{print $3, $4}' | head -c 19)
   echo "$old_time"
   #date -d "$old_time"
   sleep 2m
   new_time=$(ttnctl gateways status gatewayid --router-id ttn-router-eu | awk  '/Last/{print $3, $4}' | head -c 19)
   echo "$new_time"
   interval_m=$((($(date -ud "$new_time" +'%s') - $(date -ud "$old_time" +'%s'))/60))
   echo "$interval_m"
   if [ $interval_m -eq "0" ]
   then
      status="Down"
      echo "Gateway is Down"
   else
      status="Up"
      echo "Gateway is Up"
   fi
      export status
      if [ "$mail" != "$status" ]
      then
         ./ttn-mailer.sh
         mail=$status
         echo "mail sent"
      fi
done
